# Python 3.9 & PyQt5 GUI for Multimeter TELEDYNE T3DMM4-5 and Power Supply TTi QL564TP Control and Measurement Collection

#Multimeter IP: 192.168.0.102
#Power Supply IP: 192.168.0.101

#Helpful links for Multimeter:
#https://cdn.teledynelecroy.com/files/manuals/t3dmmseries-programming-guide.pdf

#Helpful links for Power Supply:
#http://192.168.0.1/
#http://192.168.0.101/control.cgi
#http://192.168.0.101/cmd_set.htm
#https://resources.aimtti.com/manuals/QL_Series_II-Instruction_Manual-Iss8.pdf
    

import sys
import os
from PyQt5.QtWidgets import *
import pyvisa as visa
import time
from timeit import default_timer as timer
from datetime import datetime
import threading
from PyQt5.QtCore import QThread, pyqtSignal
import matplotlib as mpl



#Measurement's Logs
class Logs:
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\Logs\\" + timestr1 + "_Log.log"


#Multimeter 
class MultimeterVoltageMeasurementParameters:
    #timestr1 = time.strftime("%Y_%m_%d_%H_%M_%S")
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    #fileName = "C:\\Users\\elwertowskaa\\Desktop\\Multimeter\\Voltage_Measurement " + timestr1 + ".log"
    #fileName = "C:\\Users\\elwertowskaa\\Desktop\\Voltage_Measurement " + datetime.now().strftime("%Y_%m_%d_%H:%M:%S") + ".log"
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\Multimeter\\" + timestr1 + "_Voltage_Measurement.log"
    
    
"""
class MultimeterCurrentMeasurementParameters:
    #timestr1 = time.strftime("%Y_%m_%d_%H_%M_%S")
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    #fileName = "C:\\Users\\elwertowskaa\\Desktop\\Multimeter\\Current_Measurement " + timestr1 + ".log"
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\Multimeter\\" + timestr1 + "_Current_Measurement.log"
"""
    
    
#Power Supply    
class PowerSupplyChannel1VoltageMeasurementParameters:
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\PowerSupplyChannel1\\" + timestr1 + "_Voltage_Measurement.log"
    
    
class PowerSupplyChannel1CurrentMeasurementParameters:
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\PowerSupplyChannel1\\" + timestr1 + "_Current_Measurement.log"
    
    
class PowerSupplyChannel2VoltageMeasurementParameters:
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\PowerSupplyChannel2\\" + timestr1 + "_Voltage_Measurement.log"
    
    
class PowerSupplyChannel2CurrentMeasurementParameters:
    timestr1 = time.strftime("%d.%m.%Y_%H.%M.%S")
    fileName = "C:\\Users\\elwertowskaa\\Desktop\\PowerSupplyChannel2\\" + timestr1 + "_Current_Measurement.log"
    


#Threads
class MeasurementReaderThread(QThread):
    multimeter_voltage_read = pyqtSignal(str)
    """multimeter_current_read = pyqtSignal(str)"""
    
    power_supply_channel1_voltage_read = pyqtSignal(str)
    power_supply_channel1_current_read = pyqtSignal(str)
    
    power_supply_channel2_voltage_read = pyqtSignal(str)
    power_supply_channel2_current_read = pyqtSignal(str)
    
    power_supply_channel1_overvoltage_read = pyqtSignal(str)
    power_supply_channel1_overcurrent_read = pyqtSignal(str)
    
    power_supply_channel2_overvoltage_read = pyqtSignal(str)
    power_supply_channel2_overcurrent_read = pyqtSignal(str)
    
    
    def __init__(self, multimeter, power_supply, parent=None): 
        super().__init__(parent)
        self.multimeter = multimeter
        self.power_supply = power_supply
        self.reading = False

    def run(self):
        while True:
            if self.reading:
                
                self.multimeter.write("CALC:AVER:AVER?")
                
                multimeterVoltage = self.multimeter.query("MEAS:VOLT?")
                #self.multimeter_voltage_read.emit(voltage + "V")
                self.multimeter_voltage_read.emit(multimeterVoltage)
                
                """
                multimeterCurrent = self.multimeter.query("MEAS:CURR?")
                #self.multimeter_current_read.emit(multimeterCurrent + "A")
                self.multimeter_current_read.emit(multimeterCurrent)
                """
                
                powersupplyChannel1Voltage = self.power_supply.query("V1O?") 
                self.power_supply_channel1_voltage_read.emit(powersupplyChannel1Voltage) 
                
                powersupplyChannel1Current = self.power_supply.query("I1O?") 
                self.power_supply_channel1_current_read.emit(powersupplyChannel1Current) 
                
                
                powersupplyChannel2Voltage = self.power_supply.query("V2O?") 
                self.power_supply_channel2_voltage_read.emit(powersupplyChannel2Voltage) 
                
                powersupplyChannel2Current = self.power_supply.query("I2O?") 
                self.power_supply_channel2_current_read.emit(powersupplyChannel2Current) 
                
                
                powersupplyChannel1OverVoltage = self.power_supply.query("LSR1?") 
                self.power_supply_channel1_overvoltage_read.emit(powersupplyChannel1OverVoltage) 
                
                powersupplyChannel1OverCurrent = self.power_supply.query("LSR1?") 
                self.power_supply_channel1_overcurrent_read.emit(powersupplyChannel1OverCurrent) 
                
                
                powersupplyChannel2OverVoltage = self.power_supply.query("LSR2?") 
                self.power_supply_channel2_overvoltage_read.emit(powersupplyChannel2OverVoltage) 
                
                powersupplyChannel2OverCurrent = self.power_supply.query("LSR2?") 
                self.power_supply_channel2_overcurrent_read.emit(powersupplyChannel2OverCurrent) 

                
                
    def start_reading(self):
        self.reading = True

    def stop_reading(self):
        self.reading = False
        
        
#GUI
class GUI(QMainWindow):
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("GUI")
        self.setGeometry(300, 150, 1000, 800)

        rm = visa.ResourceManager()
        
        #Multimeter
        self.multimeter = rm.open_resource("TCPIP0::192.168.0.102::inst0::INSTR", write_termination = '\n',read_termination='\n')
        print(self.multimeter.query("*IDN?"))
        self.multimeter.write("*RST")
        self.multimeter.write("TRIG:SOUR IMM")
        self.multimeter.write("CALC:AVER:STAT ON")
        
        #Multimeter Voltage
        self.multimeter.write("CONF:VOLT:DC AUTO")
        self.multimeter.write("VOLT:DC:NPLC 10")
        
        """
        #Multimeter Current
        self.multimeter.write("CONF:CURR:AC AUTO")
        self.multimeter.write("CURR:DC:NPLC 10")
        """
        
        #Power Supply
        self.power_supply = rm.open_resource("TCPIP::192.168.0.101::9221::SOCKET", write_termination = '\n',read_termination='\n')
        print(self.power_supply.query("*IDN?"))
        print(self.power_supply.query("EER?"))
        
        
        
        
        
        #Power Supply
        self.power_supply_label = QLabel(self)
        self.power_supply_label.setText("POWER SUPPLY")
        self.power_supply_label.move(15,10)
        
        self.power_supply_frame = QFrame(self)
        self.power_supply_frame.setFrameShape(QFrame.StyledPanel)
        self.power_supply_frame.move(5,5)
        self.power_supply_frame.resize(600,380)
        self.power_supply_frame.setLineWidth(1)
        
        
        #Line between Channel 1 and Channel 2
        self.line_frame = QFrame(self)
        self.line_frame.setFrameShape(QFrame.StyledPanel)
        self.line_frame.move(300,30)
        self.line_frame.resize(1,320)
        self.line_frame.setLineWidth(1)
        
        
        #Power Supply Channel 1
        self.power_supply_channel1_label = QLabel(self)
        self.power_supply_channel1_label.setText("CHANNEL 1")
        self.power_supply_channel1_label.move(15,30)
        
        
        #Power Supply Channel 1 Voltage Settings
        self.power_supply_channel1_vol_set_label = QLabel(self)
        self.power_supply_channel1_vol_set_label.setText("VOL SET")
        self.power_supply_channel1_vol_set_label.move(15,60)
        
        self.power_supply_channel1_vol_set_textbox = QLineEdit(self)
        self.power_supply_channel1_vol_set_textbox.move(15,90)
        self.power_supply_channel1_vol_set_textbox.resize(60,30)
        
        
        #Power Supply Channel 1 Voltage Read
        self.power_supply_channel1_vol_read_label = QLabel(self)
        self.power_supply_channel1_vol_read_label.setText("VOL READ")
        self.power_supply_channel1_vol_read_label.move(150,60)
        
        self.power_supply_channel1_vol_read_textbox = QTextEdit(self)
        self.power_supply_channel1_vol_read_textbox.setReadOnly(True)
        self.power_supply_channel1_vol_read_textbox.move(150,90)
        self.power_supply_channel1_vol_read_textbox.resize(90,30)
        
        
        #Power Supply Channel 1 Current Settings
        self.power_supply_channel1_curr_set_label = QLabel(self)
        self.power_supply_channel1_curr_set_label.setText("CURR SET")
        self.power_supply_channel1_curr_set_label.move(15,140)
        
        self.power_supply_channel1_curr_set_textbox = QLineEdit(self)
        self.power_supply_channel1_curr_set_textbox.move(15,170)
        self.power_supply_channel1_curr_set_textbox.resize(60,30)
        
        
        #Power Supply Channel 1 Current Read
        self.power_supply_channel1_curr_read_label = QLabel(self)
        self.power_supply_channel1_curr_read_label.setText("CURR READ")
        self.power_supply_channel1_curr_read_label.move(150,140)
        
        self.power_supply_channel1_curr_read_textbox = QTextEdit(self)
        self.power_supply_channel1_curr_read_textbox.setReadOnly(True)
        self.power_supply_channel1_curr_read_textbox.move(150,170)
        self.power_supply_channel1_curr_read_textbox.resize(90,30)
        
        
        #Power Supply Channel 1 Set Button
        self.power_supply_channel1_set_button = QPushButton("Set", self)
        self.power_supply_channel1_set_button.move(15,230)
        self.power_supply_channel1_set_button.clicked.connect(self.power_supply_channel1_set)
        
        #Power Supply Channel 1 Reset Button
        self.power_supply_channel1_reset_button = QPushButton("Reset", self)
        self.power_supply_channel1_reset_button.move(150,230)
        self.power_supply_channel1_reset_button.clicked.connect(self.power_supply_channel1_reset)
        
        
        #Power Supply Channel 1 Overvoltage
        self.power_supply_channel1_overvoltage_label = QLabel(self)
        self.power_supply_channel1_overvoltage_label.setText("OVERVOLTAGE?")
        self.power_supply_channel1_overvoltage_label.move(15,290)
        
        self.power_supply_channel1_overvoltage_set_textbox = QLineEdit(self)
        self.power_supply_channel1_overvoltage_set_textbox.move(120,290)
        self.power_supply_channel1_overvoltage_set_textbox.resize(60,30)
        
        self.power_supply_channel1_overvoltage_read_textbox = QTextEdit(self)
        self.power_supply_channel1_overvoltage_read_textbox.setReadOnly(True)
        self.power_supply_channel1_overvoltage_read_textbox.move(200,290)
        self.power_supply_channel1_overvoltage_read_textbox.resize(80,30)
        
        
        #Power Supply Channel 1 Overcurrent
        self.power_supply_channel1_overcurrent_label = QLabel(self)
        self.power_supply_channel1_overcurrent_label.setText("OVERCURRENT?")
        self.power_supply_channel1_overcurrent_label.move(15,340)
        
        self.power_supply_channel1_overcurrent_set_textbox = QLineEdit(self)
        self.power_supply_channel1_overcurrent_set_textbox.move(120,340)
        self.power_supply_channel1_overcurrent_set_textbox.resize(60,30)
        
        self.power_supply_channel1_overcurrent_read_textbox = QTextEdit(self)
        self.power_supply_channel1_overcurrent_read_textbox.setReadOnly(True)
        self.power_supply_channel1_overcurrent_read_textbox.move(200,340)
        self.power_supply_channel1_overcurrent_read_textbox.resize(80,30)
        
        
        
        
        
        #Power Supply Channel 2
        self.power_supply_channel2_label = QLabel(self)
        self.power_supply_channel2_label.setText("CHANNEL 2")
        self.power_supply_channel2_label.move(310,30)
        
        
        #Power Supply Channel 2 Voltage Settings
        self.power_supply_channel2_vol_set_label = QLabel(self)
        self.power_supply_channel2_vol_set_label.setText("VOL SET")
        self.power_supply_channel2_vol_set_label.move(310,60)
        
        self.power_supply_channel2_vol_set_textbox = QLineEdit(self)
        self.power_supply_channel2_vol_set_textbox.move(310,90)
        self.power_supply_channel2_vol_set_textbox.resize(60,30)
        
        
        #Power Supply Channel 2 Voltage Read
        self.power_supply_channel2_vol_read_label = QLabel(self)
        self.power_supply_channel2_vol_read_label.setText("VOL READ")
        self.power_supply_channel2_vol_read_label.move(445,60)
        
        self.power_supply_channel2_vol_read_textbox = QTextEdit(self)
        self.power_supply_channel2_vol_read_textbox.setReadOnly(True)
        self.power_supply_channel2_vol_read_textbox.move(445,90)
        self.power_supply_channel2_vol_read_textbox.resize(90,30)
        
        
        #Power Supply Channel 2 Current Settings
        self.power_supply_channel2_curr_set_label = QLabel(self)
        self.power_supply_channel2_curr_set_label.setText("CURR SET")
        self.power_supply_channel2_curr_set_label.move(310,140)
        
        self.power_supply_channel2_curr_set_textbox = QLineEdit(self)
        self.power_supply_channel2_curr_set_textbox.move(310,170)
        self.power_supply_channel2_curr_set_textbox.resize(60,30)
        
        
        #Power Supply Channel 2 Current Read
        self.power_supply_channel2_curr_read_label = QLabel(self)
        self.power_supply_channel2_curr_read_label.setText("CURR READ")
        self.power_supply_channel2_curr_read_label.move(445,140)
        
        self.power_supply_channel2_curr_read_textbox = QTextEdit(self)
        self.power_supply_channel2_curr_read_textbox.setReadOnly(True)
        self.power_supply_channel2_curr_read_textbox.move(445,170)
        self.power_supply_channel2_curr_read_textbox.resize(90,30)
        
        
        #Power Supply Channel 2 Set Button
        self.power_supply_channel2_set_button = QPushButton("Set", self)
        self.power_supply_channel2_set_button.move(310,230)
        self.power_supply_channel2_set_button.clicked.connect(self.power_supply_channel2_set)
        
        #Power Supply Channel 2 Reset Button
        self.power_supply_channel2_reset_button = QPushButton("Reset", self)
        self.power_supply_channel2_reset_button.move(445,230)
        self.power_supply_channel2_reset_button.clicked.connect(self.power_supply_channel2_reset)
        
        
        #Power Supply Channel 2 Overvoltage
        self.power_supply_channel2_overvoltage_label = QLabel(self)
        self.power_supply_channel2_overvoltage_label.setText("OVERVOLTAGE?")
        self.power_supply_channel2_overvoltage_label.move(310,290)
        
        self.power_supply_channel2_overvoltage_set_textbox = QLineEdit(self)
        self.power_supply_channel2_overvoltage_set_textbox.move(415,290)
        self.power_supply_channel2_overvoltage_set_textbox.resize(60,30)
        
        self.power_supply_channel2_overvoltage_read_textbox = QTextEdit(self)
        self.power_supply_channel2_overvoltage_read_textbox.setReadOnly(True)
        self.power_supply_channel2_overvoltage_read_textbox.move(495,290)
        self.power_supply_channel2_overvoltage_read_textbox.resize(80,30)
        
        
        #Power Supply Channel 2 Overcurrent
        self.power_supply_channel2_overcurrent_label = QLabel(self)
        self.power_supply_channel2_overcurrent_label.setText("OVERCURRENT?")
        self.power_supply_channel2_overcurrent_label.move(310,340)
        
        self.power_supply_channel2_overcurrent_set_textbox = QLineEdit(self)
        self.power_supply_channel2_overcurrent_set_textbox.move(415,340)
        self.power_supply_channel2_overcurrent_set_textbox.resize(60,30)
        
        self.power_supply_channel2_overcurrent_read_textbox = QTextEdit(self)
        self.power_supply_channel2_overcurrent_read_textbox.setReadOnly(True)
        self.power_supply_channel2_overcurrent_read_textbox.move(495,340)
        self.power_supply_channel2_overcurrent_read_textbox.resize(80,30)
        
        
        
        
        
        #Multimeter 
        self.multimeter_label = QLabel(self)
        self.multimeter_label.setText("MULTIMETER")
        self.multimeter_label.move(650,10)
        
        self.multimeter_frame = QFrame(self)
        self.multimeter_frame.setFrameShape(QFrame.StyledPanel)
        self.multimeter_frame.move(640,5)
        self.multimeter_frame.resize(300,180)
        self.multimeter_frame.setLineWidth(1)
        
        
        #Multimeter Voltage
        self.multimeter_voltage_label = QLabel(self)
        self.multimeter_voltage_label.setText("VOL: ")
        self.multimeter_voltage_label.move(650,40)
        
        self.multimeter_voltage_textbox = QTextEdit(self)
        self.multimeter_voltage_textbox.setReadOnly(True)
        self.multimeter_voltage_textbox.move(650,70)
        self.multimeter_voltage_textbox.resize(60,30)
        
        
        #Multimeter Current
        self.multimeter_current_label = QLabel(self)
        self.multimeter_current_label.setText("CURR: ")
        self.multimeter_current_label.move(800,40)
        
        self.multimeter_current_textbox = QTextEdit(self)
        self.multimeter_current_textbox.setReadOnly(True)
        self.multimeter_current_textbox.move(800,70)
        self.multimeter_current_textbox.resize(60,30)
        self.multimeter_current_textbox.setText("none")


        #Start Button
        self.threads_start_button = QPushButton("Start", self)
        self.threads_start_button.move(650,130)
        self.threads_start_button.setStyleSheet("background-color:green")
        self.threads_start_button.clicked.connect(self.start_reading)

        #Stop Button
        self.threads_stop_button = QPushButton("Stop", self)
        self.threads_stop_button.setStyleSheet("background-color:red")
        self.threads_stop_button.move(800,130)
        self.threads_stop_button.clicked.connect(self.stop_reading)
        
        
        
        
        
        #Power Supply Control Panel
        self.power_supply_control_panel_label = QLabel(self)
        self.power_supply_control_panel_label.setText("POWER SUPPLY CONTROL PANEL")
        self.power_supply_control_panel_label.move(650,215)
        self.power_supply_control_panel_label.resize(300,10)
        
        self.power_supply_control_panel_frame = QFrame(self)
        self.power_supply_control_panel_frame.setFrameShape(QFrame.StyledPanel)
        self.power_supply_control_panel_frame.move(640,200)
        self.power_supply_control_panel_frame.resize(300,180)
        self.power_supply_control_panel_frame.setLineWidth(1)
        
        
        
        #Power Supply Control Panel Channel 1 Button ON
        self.power_supply_control_panel_channel1_button_on = QPushButton("Channel 1 ON", self)
        self.power_supply_control_panel_channel1_button_on.move(650,245)
        self.power_supply_control_panel_channel1_button_on.clicked.connect(self.power_supply_control_panel_channel1_on)
        

        #Power Supply Control Panel Channel 1 Button OFF
        self.power_supply_control_panel_channel1_button_off = QPushButton("Channel 1 OFF", self)
        self.power_supply_control_panel_channel1_button_off.move(650,285)
        self.power_supply_control_panel_channel1_button_off.clicked.connect(self.power_supply_control_panel_channel1_off)
        
        
        #Power Supply Control Panel Channel 2 Button ON
        self.power_supply_control_panel_channel2_button_on = QPushButton("Channel 2 ON", self)
        self.power_supply_control_panel_channel2_button_on.move(800,245)
        self.power_supply_control_panel_channel2_button_on.clicked.connect(self.power_supply_control_panel_channel2_on)
        
        
        #Power Supply Control Panel Channel 2 Button OFF
        self.power_supply_control_panel_channel2_button_off = QPushButton("Channel 2 OFF", self)
        self.power_supply_control_panel_channel2_button_off.move(800,285)
        self.power_supply_control_panel_channel2_button_off.clicked.connect(self.power_supply_control_panel_channel2_off)
        
        
        
        #Power Supply Control Panel TRIP RESET Button
        self.power_supply_control_panel_trip_reset_button = QPushButton("TRIP RESET", self)
        self.power_supply_control_panel_trip_reset_button.move(650,335)
        self.power_supply_control_panel_trip_reset_button.clicked.connect(self.power_supply_control_panel_trip_reset)
        
        #Power Supply Control Panel RESET Button
        self.power_supply_control_panel_reset_button = QPushButton("RESET", self)
        self.power_supply_control_panel_reset_button.move(800,335)
        self.power_supply_control_panel_reset_button.clicked.connect(self.power_supply_control_panel_reset)
        
        
        
        
        
        #Huge Log Textbox
        self.huge_log_textbox = QListWidget(self)
        #self.huge_log_textbox.setReadOnly(True)
        self.huge_log_textbox.move(10,400)
        self.huge_log_textbox.resize(980,300)
        
        scroll_bar = QScrollBar(self)
        self.huge_log_textbox.setVerticalScrollBar(scroll_bar)
        self.huge_log_textbox.setAutoScroll(True)
        
        #Comment Textbox
        self.comment_textbox = QLineEdit(self)
        self.comment_textbox.move(10,720)
        self.comment_textbox.resize(700,30)
        
        
        #Send Comment Button
        self.send_comment_button = QPushButton("Send", self)
        self.send_comment_button.move(720,720)
        self.send_comment_button.clicked.connect(self.send_comment_to_huge_log_textbox)
        
        #Clear Huge Log Textbox Button
        self.clear_huge_log_textbox_button = QPushButton("Clear", self)
        self.clear_huge_log_textbox_button.move(830,720)
        self.clear_huge_log_textbox_button.clicked.connect(self.clear_huge_log_textbox)
        
        



        #Threads
        self.reader_thread = MeasurementReaderThread(self.multimeter, self.power_supply)
        self.reader_thread.start()
        
        
        self.reader_thread.multimeter_voltage_read.connect(self.multimeter_voltage_update_measurement)
        """self.reader_thread.multimeter_current_read.connect(self.multimeter_current_update_measurement)"""
        
        
        self.reader_thread.power_supply_channel1_voltage_read.connect(self.power_supply_channel1_voltage_update_measurement)
        self.reader_thread.power_supply_channel1_current_read.connect(self.power_supply_channel1_current_update_measurement)
        
        self.reader_thread.power_supply_channel2_voltage_read.connect(self.power_supply_channel2_voltage_update_measurement)
        self.reader_thread.power_supply_channel2_current_read.connect(self.power_supply_channel2_current_update_measurement)
        
        
        self.reader_thread.power_supply_channel1_overvoltage_read.connect(self.power_supply_channel1_overvoltage_update_measurement)
        self.reader_thread.power_supply_channel1_overcurrent_read.connect(self.power_supply_channel1_overcurrent_update_measurement)
        
        self.reader_thread.power_supply_channel2_overvoltage_read.connect(self.power_supply_channel2_overvoltage_update_measurement)
        self.reader_thread.power_supply_channel2_overcurrent_read.connect(self.power_supply_channel2_overcurrent_update_measurement)
        



    #THREADS' FUNCTIONS

    def start_reading(self):
        self.reader_thread.start_reading()
        
    def stop_reading(self):
        self.reader_thread.stop_reading()



    def multimeter_voltage_update_measurement(self, multimeterVoltage):
        timestr2 = time.strftime("%H:%M:%S ")
        floatVoltage = float(multimeterVoltage)
        roundVoltage = round(floatVoltage, 2)
        string1Voltage = str(roundVoltage)
        
        self.multimeter_voltage_textbox.setText(string1Voltage + 'V')
        
        formatter = mpl.ticker.EngFormatter()
        formattedVoltage = formatter(floatVoltage)
        string2Voltage = str(formattedVoltage)
        
        self.huge_log_textbox.addItem(timestr2 + ' Multimeter Voltage: ' + string2Voltage + "V")
        self.huge_log_textbox.scrollToBottom()
            
        with open(Logs.fileName, 'a', encoding = 'utf-8') as f:
            f.write(timestr2 + ' Multimeter Voltage: ' + string2Voltage + "V" + "\n")
            
        with open(MultimeterVoltageMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + multimeterVoltage + "\n")
            
    
    """
    
    def multimeter_current_update_measurement(self, multimeterCurrent):
        timestr2 = time.strftime("%H:%M:%S ")
        floatCurrent = float(multimeterCurrent)
        roundCurrent = round(floatCurrent, 2)
        string1Current = str(roundCurrent)
        
        self.multimeter_current_textbox.setText(string1Current + 'A')
        
        formatter = mpl.ticker.EngFormatter()
        formattedCurrent = formatter(floatCurrent)
        string2Current = str(formattedCurrent)
        
        self.huge_log_textbox.addItem(timestr2 + ' Multimeter Current: ' + string2Current + "A")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a', encoding = 'utf-8') as f:
            f.write(timestr2 + ' Multimeter Current: ' + string2Current + "A" + "\n")
        
        with open(MultimeterCurrentMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + multimeterCurrent + "\n")
   
    """
    
    
    
    
    def power_supply_channel1_voltage_update_measurement(self, powersupplyChannel1Voltage):
        timestr2 = time.strftime("%H:%M:%S ")
        self.power_supply_channel1_vol_read_textbox.setText(powersupplyChannel1Voltage)
        
        self.huge_log_textbox.addItem(timestr2 + ' Power Supply Channel 1 Voltage: ' + powersupplyChannel1Voltage)
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            #f.write(timestr2 + ' Power Supply Channel 1 Voltage: ' + powersupplyChannel1Voltage + "\n")
            f.write(timestr2 + ' Power Supply Channel 1 Voltage: ' + powersupplyChannel1Voltage)
                
        with open(PowerSupplyChannel1VoltageMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + powersupplyChannel1Voltage)
            
            
            
    def power_supply_channel1_current_update_measurement(self, powersupplyChannel1Current):
        timestr2 = time.strftime("%H:%M:%S ")
        self.power_supply_channel1_curr_read_textbox.setText(powersupplyChannel1Current)
        
        self.huge_log_textbox.addItem(timestr2 + ' Power Supply Channel 1 Current: ' + powersupplyChannel1Current)
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            #f.write(timestr2 + ' Power Supply Channel 1 Current: ' + powersupplyChannel1Current + "\n")
            f.write(timestr2 + ' Power Supply Channel 1 Current: ' + powersupplyChannel1Current)
            
        with open(PowerSupplyChannel1CurrentMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + powersupplyChannel1Current)
    
    
    
    
            
    def power_supply_channel2_voltage_update_measurement(self, powersupplyChannel2Voltage):
        timestr2 = time.strftime("%H:%M:%S ")
        self.power_supply_channel2_vol_read_textbox.setText(powersupplyChannel2Voltage)
        
        self.huge_log_textbox.addItem(timestr2 + ' Power Supply Channel 2 Voltage: ' + powersupplyChannel2Voltage)
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            #f.write(timestr2 + ' Power Supply Channel 2 Voltage: ' + powersupplyChannel2Voltage + "\n")
            f.write(timestr2 + ' Power Supply Channel 2 Voltage: ' + powersupplyChannel2Voltage)
            
        with open(PowerSupplyChannel2VoltageMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + powersupplyChannel2Voltage)
                    
            
            
    def power_supply_channel2_current_update_measurement(self, powersupplyChannel2Current):
        timestr2 = time.strftime("%H:%M:%S ")
        self.power_supply_channel2_curr_read_textbox.setText(powersupplyChannel2Current)
        
        self.huge_log_textbox.addItem(timestr2 + ' Power Supply Channel 2 Current: ' + powersupplyChannel2Current)
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            #f.write(timestr2 + ' Power Supply Channel 2 Current: ' + powersupplyChannel2Current + "\n")
            f.write(timestr2 + ' Power Supply Channel 2 Current: ' + powersupplyChannel2Current)
            
        with open(PowerSupplyChannel2CurrentMeasurementParameters.fileName, 'a') as f:
            f.write(timestr2 + ' ' + powersupplyChannel2Current)
            
            
    
    
            
    def power_supply_channel1_overvoltage_update_measurement(self, powersupplyChannel1OverVoltage):
        timestr2 = time.strftime("%H:%M:%S ")
        
        intChannel1OverVoltage = int(powersupplyChannel1OverVoltage)
        
        Channel1OverVoltage = intChannel1OverVoltage >> (3 - 1)
        
        if Channel1OverVoltage & 1:
            
            self.power_supply_channel1_overvoltage_read_textbox.setText('YES')
            self.huge_log_textbox.addItem(timestr2 + ' OVERVOLTAGE CHANNEL 1 ALARM!')
            self.huge_log_textbox.scrollToBottom()
        
            with open(Logs.fileName, 'a') as f:
                f.write(timestr2 + ' OVERVOLTAGE CHANNEL 1 ALARM!')
                
        #else:
            
            #self.power_supply_channel1_overvoltage_read_textbox.setText(' ')
            
    
    
    def power_supply_channel1_overcurrent_update_measurement(self, powersupplyChannel1OverCurrent):
        timestr2 = time.strftime("%H:%M:%S ")
        
        #self.huge_log_textbox.addItem(timestr2 + ' OVERCURRENT CHANNEL 1: ' + powersupplyChannel1OverCurrent)
        
        intChannel1OverCurrent = int(powersupplyChannel1OverCurrent)
        
        Channel1OverCurrent = intChannel1OverCurrent >> (4 - 1)
        
        if Channel1OverCurrent & 1:
            
            self.power_supply_channel1_overcurrent_read_textbox.setText('YES')
            self.huge_log_textbox.addItem(timestr2 + ' OVERCURRENT CHANNEL 1 ALARM!')
            self.huge_log_textbox.scrollToBottom()
            
            with open(Logs.fileName, 'a') as f:
                f.write(timestr2 + ' OVERCURRENT CHANNEL 1 ALARM!')
        
        #else:
            
            #self.power_supply_channel1_overcurrent_read_textbox.setText(' ')
        
        
        
        
        
    def power_supply_channel2_overvoltage_update_measurement(self, powersupplyChannel2OverVoltage):
        timestr2 = time.strftime("%H:%M:%S ")
        
        intChannel2OverVoltage = int(powersupplyChannel2OverVoltage)
        
        Channel2OverVoltage = intChannel2OverVoltage >> (3 - 1)
    
        if Channel2OverVoltage & 1:
    
            self.power_supply_channel2_overvoltage_read_textbox.setText('YES')
            self.huge_log_textbox.addItem(timestr2 + ' OVERVOLTAGE CHANNEL 2 ALARM!')
            self.huge_log_textbox.scrollToBottom()
            
            with open(Logs.fileName, 'a') as f:
                f.write(timestr2 + ' OVERVOLTAGE CHANNEL 2 ALARM!')
    
        #else:
            
            #self.power_supply_channel2_overvoltage_read_textbox.setText(' ')
    
    
    
    def power_supply_channel2_overcurrent_update_measurement(self, powersupplyChannel2OverCurrent):
        timestr2 = time.strftime("%H:%M:%S ")
        
        intChannel2OverCurrent = int(powersupplyChannel2OverCurrent)
       
        Channel2OverCurrent = intChannel2OverCurrent >> (4 - 1)
        
        if Channel2OverCurrent & 1:
            
            self.power_supply_channel2_overcurrent_read_textbox.setText('YES')
            self.huge_log_textbox.addItem(timestr2 + ' OVERCURRENT CHANNEL 2 ALARM!')
            self.huge_log_textbox.scrollToBottom()
            
            with open(Logs.fileName, 'a') as f:
                f.write(timestr2 + ' OVERCURRENT CHANNEL 2 ALARM!')
        
        #else:
            
            #self.power_supply_channel2_overcurrent_read_textbox.setText(' ')

    
    
    
    
    #BUTTONS' FUNCTIONS
    
    def send_comment_to_huge_log_textbox(self):
        timestr2 = time.strftime("%H:%M:%S ")
        comment = self.comment_textbox.text()
        self.huge_log_textbox.addItem(timestr2 + ' ' + comment)
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + ' ' + comment + "\n")
        
        
        
    def clear_huge_log_textbox(self):
        self.huge_log_textbox.clear()
        
        
        
    def power_supply_channel1_set(self):
        Channel1Vol = self.power_supply_channel1_vol_set_textbox.text()
        self.power_supply.write("V1 " + Channel1Vol)
        
        Channel1Curr = self.power_supply_channel1_curr_set_textbox.text()
        self.power_supply.write("I1 " + Channel1Curr)
        
        
        Channel1OverVol = self.power_supply_channel1_overvoltage_set_textbox.text()
        self.power_supply.write("OVP1 " + Channel1OverVol)
        
        Channel1OverCurr = self.power_supply_channel1_overcurrent_set_textbox.text()
        self.power_supply.write("OCP1 " + Channel1OverCurr)
        
        time.sleep(1)
        
        newChannel1Vol = self.power_supply.query("V1O?")
        self.power_supply_channel1_vol_read_textbox.setText(newChannel1Vol)
        
        newChannel1Curr = self.power_supply.query("I1O?")
        self.power_supply_channel1_curr_read_textbox.setText(newChannel1Curr)
        
        
        newChannel1OverVol = self.power_supply.query("OVP1?")
        self.power_supply_channel1_overvoltage_read_textbox.setText(newChannel1OverVol)
        
        newChannel1OverCurr = self.power_supply.query("OCP1?")
        self.power_supply_channel1_overcurrent_read_textbox.setText(newChannel1OverCurr)
    
    
        newMultimeterVol = self.multimeter.query("MEAS:VOLT?")
        floatMultimeterVol = float(newMultimeterVol)
        roundMultimeterVol = round(floatMultimeterVol, 2)
        string1MultimeterVol = str(roundMultimeterVol)
        self.multimeter_voltage_textbox.setText(string1MultimeterVol + 'V')
        
        '''
        newMultimeterCurr = self.multimeter.query("MEAS:CURR?")
        floatMultimeterCurr = float(newMultimeterCurr)
        roundMultimeterCurr = round(floatMultimeterCurr, 2)
        string1MultimeterCurr = str(roundMultimeterCurr)
        self.multimeter_current_textbox.setText(string1MultimeterCurr + 'A')
        '''
        
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 1 Voltage to " + Channel1Vol + " V")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 1 Current to " + Channel1Curr + " A")
        
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 1 OverVoltage to " + Channel1OverVol + " V")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 1 OverCurrent to " + Channel1OverCurr + " A")
        
        self.huge_log_textbox.scrollToBottom()
        
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Changed Channel 1 Voltage to " + Channel1Vol + " V" + "\n")
            f.write(timestr2 + " Changed Channel 1 Current to " + Channel1Curr + " A" + "\n")
            
            f.write(timestr2 + " Changed Channel 1 OverVoltage to " + Channel1OverVol + " V" + "\n")
            f.write(timestr2 + " Changed Channel 1 OverCurrent to " + Channel1OverCurr + " A" + "\n")
            
        
        
    def power_supply_channel2_set(self):
        Channel2Vol = self.power_supply_channel2_vol_set_textbox.text()
        self.power_supply.write("V2 " + Channel2Vol)
        
        Channel2Curr = self.power_supply_channel2_curr_set_textbox.text()
        self.power_supply.write("I2 " + Channel2Curr)
        
        
        Channel2OverVol = self.power_supply_channel2_overvoltage_set_textbox.text()
        self.power_supply.write("OVP2 " + Channel2OverVol)
        
        Channel2OverCurr = self.power_supply_channel2_overcurrent_set_textbox.text()
        self.power_supply.write("OCP2 " + Channel2OverCurr)
        
        time.sleep(1)
        
        newChannel2Vol = self.power_supply.query("V2O?")
        self.power_supply_channel2_vol_read_textbox.setText(newChannel2Vol)
        
        newChannel2Curr = self.power_supply.query("I2O?")
        self.power_supply_channel2_curr_read_textbox.setText(newChannel2Curr)
        
        
        newChannel2OverVol = self.power_supply.query("OVP2?")
        self.power_supply_channel2_overvoltage_read_textbox.setText(newChannel2OverVol)
        
        newChannel2OverCurr = self.power_supply.query("OCP2?")
        self.power_supply_channel2_overcurrent_read_textbox.setText(newChannel2OverCurr)
        
        
        newMultimeterVol = self.multimeter.query("MEAS:VOLT?")
        floatMultimeterVol = float(newMultimeterVol)
        roundMultimeterVol = round(floatMultimeterVol, 2)
        string1MultimeterVol = str(roundMultimeterVol)
        self.multimeter_voltage_textbox.setText(string1MultimeterVol + 'V')
        
        '''
        newMultimeterCurr = self.multimeter.query("MEAS:CURR?")
        floatMultimeterCurr = float(newMultimeterCurr)
        roundMultimeterCurr = round(floatMultimeterCurr, 2)
        string1MultimeterCurr = str(roundMultimeterCurr)
        self.multimeter_current_textbox.setText(string1MultimeterCurr + 'A')
        '''
        
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 2 Voltage to " + Channel2Vol + " V")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 2 Current to " + Channel2Curr + " A")
        
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 2 OverVoltage to " + Channel2OverVol + " V")
        self.huge_log_textbox.addItem(timestr2 + " Changed Channel 2 OverCurrent to " + Channel2OverCurr + " A")
        
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Changed Channel 2 Voltage to " + Channel2Vol + " V" + "\n")
            f.write(timestr2 + " Changed Channel 2 Current to " + Channel2Curr + " A" + "\n")
            
            f.write(timestr2 + " Changed Channel 2 OverVoltage to " + Channel2OverVol + " V" + "\n")
            f.write(timestr2 + " Changed Channel 2 OverCurrent to " + Channel2OverCurr + " A" + "\n")
            
            
            
            
            
    def power_supply_channel1_reset(self):
        self.power_supply.write("V1 0")
        self.power_supply.write("I1 0")
        
        self.power_supply_channel1_vol_set_textbox.clear()
        self.power_supply_channel1_curr_set_textbox.clear()
        
        time.sleep(1)
        
        newChannel1Vol = self.power_supply.query("V1O?")
        self.power_supply_channel1_vol_read_textbox.setText(newChannel1Vol)
        
        newChannel1Curr = self.power_supply.query("I1O?")
        self.power_supply_channel1_curr_read_textbox.setText(newChannel1Curr)
        
        
        newMultimeterVol = self.multimeter.query("MEAS:VOLT?")
        floatMultimeterVol = float(newMultimeterVol)
        roundMultimeterVol = round(floatMultimeterVol, 2)
        string1MultimeterVol = str(roundMultimeterVol)
        self.multimeter_voltage_textbox.setText(string1MultimeterVol + 'V')
        
        '''
        newMultimeterCurr = self.multimeter.query("MEAS:CURR?")
        floatMultimeterCurr = float(newMultimeterCurr)
        roundMultimeterCurr = round(floatMultimeterCurr, 2)
        string1MultimeterCurr = str(roundMultimeterCurr)
        self.multimeter_current_textbox.setText(string1MultimeterCurr + 'A')
        '''
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Reset of Channel 1")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Reset of Channel 1" + "\n")
        
        
    
    def power_supply_channel2_reset(self):
        self.power_supply.write("V2 0")
        self.power_supply.write("I2 0")
        
        self.power_supply_channel2_vol_set_textbox.clear()
        self.power_supply_channel2_curr_set_textbox.clear()
        
        time.sleep(1)
        
        newChannel2Vol = self.power_supply.query("V2O?")
        self.power_supply_channel2_vol_read_textbox.setText(newChannel2Vol)
        
        newChannel2Curr = self.power_supply.query("I2O?")
        self.power_supply_channel2_curr_read_textbox.setText(newChannel2Curr)
        
        
        newMultimeterVol = self.multimeter.query("MEAS:VOLT?")
        floatMultimeterVol = float(newMultimeterVol)
        roundMultimeterVol = round(floatMultimeterVol, 2)
        string1MultimeterVol = str(roundMultimeterVol)
        self.multimeter_voltage_textbox.setText(string1MultimeterVol + 'V')
        
        '''
        newMultimeterCurr = self.multimeter.query("MEAS:CURR?")
        floatMultimeterCurr = float(newMultimeterCurr)
        roundMultimeterCurr = round(floatMultimeterCurr, 2)
        string1MultimeterCurr = str(roundMultimeterCurr)
        self.multimeter_current_textbox.setText(string1MultimeterCurr + 'A')
        '''
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Reset of Channel 2")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Reset of Channel 2" + "\n")
            
            
            
            
            
    def power_supply_control_panel_channel1_on(self):
        self.power_supply.write("OP1 1")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Channel 1 ON")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Channel 1 ON" + "\n")
        
        
    def power_supply_control_panel_channel1_off(self):
        self.power_supply.write("OP1 0")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Channel 1 OFF")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Channel 1 OFF" + "\n")
        
        
        
        
        
    def power_supply_control_panel_channel2_on(self):
        self.power_supply.write("OP2 1")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Channel 2 ON")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Channel 2 ON" + "\n")
        
        
    def power_supply_control_panel_channel2_off(self):
        self.power_supply.write("OP2 0")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " Channel 2 OFF")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " Channel 2 OFF" + "\n")
        
        
        
        
        
    def power_supply_control_panel_trip_reset(self):
        self.power_supply.write("TRIPRST")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " POWER SUPPLY ALL CHANNELS TRIP RESET")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " POWER SUPPLY ALL CHANNELS TRIP RESET" + "\n")
        
        
        
    def power_supply_control_panel_reset(self):
        self.power_supply.write("*RST")
        
        timestr2 = time.strftime("%H:%M:%S ")
        self.huge_log_textbox.addItem(timestr2 + " POWER SUPPLY ALL CHANNELS RESET")
        self.huge_log_textbox.scrollToBottom()
        
        with open(Logs.fileName, 'a') as f:
            f.write(timestr2 + " POWER SUPPLY ALL CHANNELS RESET" + "\n")
        
        
    
        
    
    def closeEvent(self, event):
        odp = QMessageBox.question(
                self, 'Message Box',
                "Do you want to close the application?",
                QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if odp == QMessageBox.Yes:
            try:
                self.reader_thread.stop_reading()
                self.power_supply.close()
                self.multimeter.close()
            except:
                pass
            event.accept()
        else:
            event.ignore()
        
        



app = QApplication(sys.argv)
window = GUI()
window.show()
sys.exit(app.exec_())
